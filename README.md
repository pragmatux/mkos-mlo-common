# mkos-mlo-common

A Pragmatux mkos frontend for targets that boot using MLO.

## Theory of Operation

The mkos-mlo-common concept is just a wrapper around the `multistrap` tool. The
`mkos-mlo-config` tool generates a multistrap configuration file from a
template, and then various multistrap hooks are employed to create a bootable
SD-card image file. Other artifacts include a filesystem directory, and an
APT-compatible repository in both repo and ISO formats. A copy of the actual
multistrap configuration file used to produce these work products is also
preserved.

## Example

The typical user will invoke us as follows:

```bash
#!/bin/bash
#set -e -u

device="boneblack"
codename="unstable"

ARGS="$@"

tempcfg="mkos-${device}.mstrap"
tempmount="mkos-${device}.fs"
cfgfile=/usr/share/mkos-${device}/$tempcfg

# Generate the multistrap config file.
mkos-mlo-config -i $cfgfile -o $tempcfg -t mkos-${device}.tar.gz -p device-${device} $ARGS

# Run the pre-multistrap hooks, then produce the filesystem directory.
#
# Additional hooks inside of multistrap will create the tgz, repo, manifest,
# ISO, and IMG files.
mkos-mlo-prehooks $tempmount $@ &&
    fakeroot /usr/sbin/multistrap -f $tempcfg -d $tempmount
```

## Build Instructions

```console
user@workstation:~$ cd mkos-boneblack
user@workstation:~$ ptuxbuild
```

The build products are found in `debian/build/`.
