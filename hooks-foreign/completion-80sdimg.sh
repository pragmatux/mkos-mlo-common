#!/bin/bash
[ "$DEBUG" == "true" ] && set -x

if [[ -z "$1" ]]; then
    echo "${0##*/} <path-to-filesystem-directory>"
    exit 0
fi

mnt=$1
img=${mnt%.fs/}.img
repo=${mnt%.fs/}.repo
rootfstype=${ROOTFSTYPE:-"ext4"}

# Format and populate the image file.
guestfish --no-progress-bars -a $img <<EOF
run
list-partitions
mkfs-opts $rootfstype /dev/sda1
mount /dev/sda1 /
lcd $mnt
copy-in . /
lcd $repo
mkdir /mnt
mkdir /mnt/repo
copy-in . /mnt/repo
EOF

echo "SD image located in '$img'"
