#!/bin/bash

# Warns if the filesystem directory already exists. Running multistrap with an
# already-populated output directory is known to cause problems.
#
# Exit 1 means "it's not ok to proceed".

if [[ -z "$1" ]]; then
    # We can't "check" an unknown location.
    echo "${0##*/} <path-to-filesystem-directory>"
    exit 1
fi

if [[ -e "$1" ]]; then
    echo "${0##*/}:E: '$1' already exists, not overwriting it."
    exit 1
fi
