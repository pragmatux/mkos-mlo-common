#!/bin/bash

# Builds a package manifest from the packages installed in the filesystem.
#

if [[ -z "$1" ]]; then
    echo "${0##*/} <path-to-filesystem-directory>"
    exit 0
fi

mnt=$1
manifest=${mnt%.fs/}.manifest

# dpkg-query might work better here.
dpkg --root=$mnt -l > $manifest
echo "Package manifest recorded in '$manifest'"

