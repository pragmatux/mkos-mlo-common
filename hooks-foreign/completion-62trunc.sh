#!/bin/bash
[ "$DEBUG" == "true" ] && set -x

if [[ -z "$1" ]]; then
    echo "${0##*/} <path-to-filesystem-directory>"
    exit 0
fi

# Use truncate(1) to create an empty file of the requested size.
mnt=$1
img=${mnt%.fs/}.img
truncate --size=$(echo ${FSSIZEMB:-1024}*1024*1024 | bc) $img
