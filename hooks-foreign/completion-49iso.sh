#!/bin/bash

# Generates an ISO image containing an APT repository.
#

if [[ -z "$1" ]]; then
    echo "${0##*/} <path-to-filesystem-directory>"
    exit 0
fi

mnt=$1
repo=${mnt%.fs/}.repo
iso=${mnt%.fs/}.iso

genisoimage -input-charset utf-8 -output-charset utf-8 -quiet -o $iso -r $repo
echo "Package repository ISO located in '$iso'"
