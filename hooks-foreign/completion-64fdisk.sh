#!/bin/bash
[ "$DEBUG" == "true" ] && set -x

if [[ -z "$1" ]]; then
    echo "${0##*/} <path-to-filesystem-directory>"
    exit 0
fi

mnt=$1
img=${mnt%.fs/}.img
rootfstype=${ROOTFSTYPE:-"ext4"}

# Translate rootfstype to its corresponding code
case "$rootfstype" in
    ext4|ext3|ext2|0x83)
	 rootfstypeid="0x83"
	 ;;
    *)
	 echo "Unknown filesystem type: $rootfstype"
        exit 1
	 ;;
esac

/sbin/sfdisk $img <<EOF
start=4096 type=$rootfstypeid bootable
EOF
