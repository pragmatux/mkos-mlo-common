#!/bin/bash
[ "$DEBUG" == "true" ] && set -x

# Builds a package repository from the packages installed in the filesystem.
#

if [[ -z "$1" ]]; then
    echo "${0##*/} <path-to-filesystem-directory>"
    exit 0
fi

mnt=$1
repo=${mnt%.fs/}.repo

# TODO: Make codename follow settings specified elsewhere.
codename="unstable"

mkdir -p $repo/conf
cat > $repo/conf/distributions <<EOF
Codename: $codename
Components: main
Architectures: armhf
Description: mkos-device repository
EOF
REPREPRO_BASE_DIR=$repo reprepro --silent includedeb $codename $mnt/var/cache/apt/archives/*deb

# We don't need the the package archive directory anymore.
rm -f $mnt/var/cache/apt/archives/*deb

echo "Package repository installed in '$repo'"

