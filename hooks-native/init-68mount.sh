#!/bin/bash
[ "$DEBUG" == "true" ] && set -x

if [[ -z "$DEV" ]]; then
    exit 1
fi

mount $DEV $1
