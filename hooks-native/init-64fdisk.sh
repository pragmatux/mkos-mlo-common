#!/bin/bash
[ "$DEBUG" == "true" ] && set -x

if [[ -z "$DEV" ]]; then
    exit 1
fi


if [[ -z "$1" ]]; then
    echo "${0##*/} <path-to-filesystem-directory>"
    exit 0
fi

# Note: We assume that DEV is of the form: /dev/mmcblk<B>p<N>
dev=${DEV%p*}
rootfstype=${ROOTFSTYPE:-"ext4"}

case "$rootfstype" in
    ext4|ext3|ext2|0x83)
	 rootfstypeid="0x83"
	 ;;
    *)
	 echo "Unknown filesystem type: $rootfstype"
        exit 1
	 ;;
esac

/sbin/sfdisk $dev <<EOF
start=4096 type=$rootfstypeid bootable
EOF
