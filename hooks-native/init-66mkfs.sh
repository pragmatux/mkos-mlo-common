#!/bin/bash
[ "$DEBUG" == "true" ] && set -x

if [[ -z "$DEV" ]]; then
    exit 1
fi

rootfstype=${ROOTFSTYPE:-"ext4"}
mkfs.$rootfstype -F $DEV
