#!/bin/bash
[ "$DEBUG" == "true" ] && set -x

if [[ -z "$1" ]]; then
    echo "${0##*/} <path-to-filesystem-directory>"
    exit 0
fi

mnt=$1
if [[ -z "$DEV" ]]; then
    # Writing to an image file.
    img=${mnt%.fs/}.img
else
    # Writing to a device.
    img=${DEV%p*}
fi    

# Write the SPL/MLO/u-boot images to the right places in the storage device.
#
# Note: don't change these magic numbers unless you know what you're doing.
# ref: http://www.ti.com/lit/ug/spruh73q/spruh73q.pdf
# ref: https://stackoverflow.com/questions/60873038/how-to-write-new-mlo-and-u-boot-img-to-an-sd-card-without-erasing-the-os
dd if=$mnt/boot/MLO of=$img bs=128k seek=1 count=2 conv=notrunc \
    && dd if=$mnt/boot/u-boot-dtb.img of=$img bs=384k seek=1 count=4 conv=notrunc
