#!/bin/bash
[ "$DEBUG" == "true" ] && set -x

# Generates an SD-compatible bootscript:
#   * using the target's own update-bootscr, if present; or,
#   * by copying from boot-sd.scr.
#
# If there's no boot.scr at the end of our efforts, issue a warning.

if [[ -z "$1" ]]; then
    echo "${0##*/} <path-to-filesystem-directory>"
    exit 0
fi

cd $1

bootscr=""
for scr in {boot,usr/sbin}/update-bootscr; do
    if [[ -x $scr ]]; then bootscr=$scr; fi
done
UNAME="$(find boot -name 'vmlinuz-*' -type f | sort -n | tail -n 1 | sed 's%boot/vmlinuz-%%g')"
if [[ -x $bootscr && ! -z "$UNAME" ]]; then
    DEV=${DEV:-mmcblk0p1} INFILE=boot/boot.txt.in OUTFILE=boot/boot.scr UNAME=$UNAME $bootscr
fi

# If there's no boot.scr, but there's a boot-sd.scr, copy the latter to the
# former. This will be the case for targets that can boot from SD and/or eMMC
# and that use the same boot-script-containing package for both configurations,
# i.e. they don't have an update-bootscr.
if [[ ! -f "boot/boot.scr" && -f "boot/boot-sd.scr" ]] ; then
    cp boot/boot-sd.scr boot/boot.scr
fi

# Warn if we couldn't figure out how this image will boot.
if [ ! -f "boot/boot.scr" ] ; then
    echo W: No boot.scr found, this image may not be bootable.
fi
