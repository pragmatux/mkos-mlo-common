#!/bin/bash

# Makes sure /var/run is a symlink to /run, and not a directory. Later versions
# of dbus, etc. depend on this.
#

if [[ -z "$1" ]]; then
    echo "${0##*/} <path-to-filesystem-directory>"
    exit 0
fi

cd $1/var && rm -rf run && ln -s /run
